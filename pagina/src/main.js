import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
/* import Icon from 'vue-awesome/components/Icon' */

/* import 'vue-awesome/icons' */

Vue.use(Vuetify)
Vue.config.productionTip = true

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
/* Vue.component('icon', Icon) */
