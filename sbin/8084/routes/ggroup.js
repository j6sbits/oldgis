var express = require('express')
var router = express.Router()

var User = require('../models/Group');

router.post('/Select/', function (req, res) {
    var Data = (req.body);

    Group.findOne({ Name: Data.Name })
    .then(Result => {
        res.json(Result);
    });
});

router.post('/Insert/', function (req, res) {
    var Data = (req.body);

    Group.create({ Name: Data.Name })
    .then(() => {
        res.json({Result: 1});
    });
});

router.post('/Delete/', function (req, res) {
    var Data = (req.body);

    Group.findOne({ Name: Data.Name })
    .then(Result => {
        Result.destroy();
        res.json({Result: 1});
    });
});

module.exports = router
